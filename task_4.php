<?php
$integers = [];

for ($i = 0; $i < rand(3, 15); $i++) {
    $integers[] = rand(3, 100);
}
$biggest = 0;
$count = 0;

foreach ($integers as $key => $value) {
    if ($biggest < $value) {
        $biggest = $value;
        $count = $key;
    }
}

var_export($integers);
print "Biggest integer is {$biggest} and number of integers before is {$count}";

