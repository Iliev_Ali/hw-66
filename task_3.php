<?php
require_once 'vendor/autoload.php';
$faker = Faker\Factory::create();

echo $str1 = $faker->sentence() . "\n" . $str2 = $faker->sentence() . "\n";

$len = min(strlen($str1), strlen($str2));
$count = 0;

for ($i = 0; $i < $len; $i++) {
    if ($str1[$i] != $str2[$i]) {
        break;
    }
    $count++;
}

if ($count == $len && strlen($str1) == strlen($str2)) {
    print "Строки совпадают полностью";
} else {
    print "Количество совпадающих начальных символов {$count}";
}






