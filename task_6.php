<?php

$array = [];
$size = 5;

for ($i = 0; $i < $size; $i++) {
    echo "Введите элемент массива №" . $i+1  . ": ";
    $array[] = (int)(trim(fgets(STDIN)));
}

$d = $array[1] - $array[0];

for ($i = 2; $i < count($array); $i++) {
    if ($array[$i] - $array[$i - 1] != $d) {
        print "NULL\n";
        die();
    }
}

print "{$d}\n";



