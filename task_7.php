<?php
echo 'Введите число: ';
$int = (int)(trim(fgets(STDIN)));
$n = intval($int / 2) + 1;
$count = 0;
$sum = 0;

for ($i = 1; $i <= $n; $i++) {

    for ($j = $i; $j <= $n; $j++) {

        $sum += $j;
        if ($sum == $int) {
            $count++;

            for ($k = $i; $k <= $j; $k++) {
                if ($k != $j) {
                    print "{$k} + ";
                } else {
                    print "{$k} = {$int}\n";
                }
            }
        }
    }
    $sum = 0;
}

echo $count . "\n";